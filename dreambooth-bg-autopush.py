#@title Save your newly created concept? you may save it privately to your personal profile or collaborate to the [library of concepts](https://huggingface.co/sd-dreambooth-library)?
#@markdown If you wish your model to be avaliable for everyone, add it to the public library. If you prefer to use your model privately, add your own profile.

import os
import sys
import subprocess
import time
import pathlib
import glob


output_dir = sys.argv[1]
name_of_your_concept = sys.argv[2]
hf_token_write = sys.argv[3]
instance_prompt = sys.argv[4]



from slugify import slugify
from huggingface_hub import HfApi, HfFolder, CommitOperationAdd
from huggingface_hub import create_repo
from IPython.display import display_markdown


if(not hf_token_write):
  with open(HfFolder.path_token, 'r') as fin: hf_token = fin.read();
else:
  hf_token = hf_token_write


api = HfApi(token=hf_token)
your_username = api.whoami()["name"]

repo_id = f"{your_username}/{name_of_your_concept}"

print('Set up autopush to repo ' + repo_id + ' from folder ' + output_dir)


readme_text = f'''  ---
license: creativeml-openrail-m
tags:
- text-to-image
---
  ### {name_of_your_concept} on Stable Diffusion via Dreambooth
  #### model by {api.whoami()["name"]}
  This your the Stable Diffusion model fine-tuned the {name_of_your_concept} concept taught to Stable Diffusion with Dreambooth.
  #It can be used by modifying the `instance_prompt`: **{instance_prompt}**

  You can also train your own concepts and upload them to the library by using [this notebook](https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/sd_dreambooth_training.ipynb).
  And you can run your new concept via `diffusers`: [Colab Notebook for Inference](https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/sd_dreambooth_inference.ipynb), [Spaces with the Public Concepts loaded](https://huggingface.co/spaces/sd-dreambooth-library/stable-diffusion-dreambooth-concepts)
'''
#Save the readme to a file
readme_file = open("README.md", "w")
readme_file.write(readme_text)
readme_file.close()
#Save the token identifier to a file
text_file = open("token_identifier.txt", "w")
text_file.write(instance_prompt)
text_file.close()

while not os.path.exists(os.path.join(output_dir,'vae/config.json')) :
	time.sleep(120)


operations = [
  CommitOperationAdd(path_in_repo="token_identifier.txt", path_or_fileobj="token_identifier.txt"),
  CommitOperationAdd(path_in_repo="README.md", path_or_fileobj="README.md"),
]
try:
  create_repo(repo_id,private=True, token=hf_token)
except:
  print('Repo already exists!')

api.create_commit(
  repo_id=repo_id,
  operations=operations,
  commit_message=f"Upload the concept {name_of_your_concept} embeds and token",
  token=hf_token
)

samples_path = os.path.join(output_dir,'samples')
pathlib.Path(samples_path).mkdir(parents=True, exist_ok=True)

#Due to sample generation we may need to upload a bit more times
for i in range(8):
	# Collect samples
	try:
		signature = "/content/stable_diffusion_weights/sample/" + output_dir[-6:] + "_*"
		for img in glob.glob(signature):
			os.link(img, samples_path + "/" + img.split("/")[-1])
	except:
		pass

	# The very uploading
	try:
		api.upload_folder(
		  folder_path=output_dir,
		  path_in_repo="",
		  repo_id=repo_id,
		  token=hf_token
		)
	except:
		print('An error occured while pushing to '+ repo_id)
		time.sleep(300)

	# Deduplicate VAE
	try:
		os.system("apt install rdfind")
		os.system("rdfind -makehardlinks true -outputname rdfind.log /content/stable_diffusion_weights/*/vae")
	except:
		print('An error occured while trying to deduplicate VAEs')

	time.sleep(60)
